//
//  ViewController.swift
//  Calculator
//
//  Created by Soeng Saravit on 10/25/17.
//  Copyright © 2017 Soeng Saravit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var currentNumber: UILabel!
    var numberOnscreen:Double = 0
    var performingMath:Bool = false
    var preNumber:Double = 0
    var `operator`:Int = 0
    
    @IBAction func numbers(_ sender: UIButton) {
        
        if performingMath == true {
            currentNumber.text =  currentNumber.text! + String(sender.tag - 1)
            numberOnscreen = Double(currentNumber.text!)!
            performingMath = false
        }
        else {
            currentNumber.text =  currentNumber.text! + String(sender.tag - 1)
            
            numberOnscreen = Double(currentNumber.text!)!
            
        }
        
    }
    @IBAction func buttonOperator(_ sender: UIButton) {
        if  sender.tag != 11 && sender.tag != 18
        {
            preNumber = Double(currentNumber.text!)!
            if sender.tag == 13
            {
                currentNumber.text! += "%"
                result.text = String(currentNumber.text!)
                currentNumber.text = ""
                
            }
            else if sender.tag == 14
            {
                currentNumber.text! += "÷"
                result.text = String(currentNumber.text!)
                currentNumber.text = ""
            }
            else if sender.tag == 15
            {
                currentNumber.text! += "x"
                result.text = String(currentNumber.text!)
            }
            else if sender.tag == 16
            {
                currentNumber.text! += "-"
                result.text = String(currentNumber.text!)
                currentNumber.text = ""
            }
            else if sender.tag == 17
            {
                currentNumber.text! += "+"
                result.text = String(currentNumber.text!)
                currentNumber.text = ""
            }
            else if sender.tag == 19
            {
                currentNumber.text! += "."
                result.text = String(currentNumber.text!)
                currentNumber.text = ""
            }
            `operator` = sender.tag
            performingMath = true
            
        }
            
            
        else if sender.tag == 18 {
            if sender.tag == 14
            {
                
                result.text = String(preNumber / numberOnscreen)
                
                currentNumber.text = ""
            }
                
            else if sender.tag == 15
            {result.text = String(preNumber * numberOnscreen)
                currentNumber.text = ""
            }
            else if sender.tag == 16
            {result.text = String(preNumber - numberOnscreen)
                currentNumber.text = ""
            }
            else if sender.tag == 17
            {result.text = String(preNumber + numberOnscreen)
                currentNumber.text = ""
            }
        }
        else if sender.tag == 11 {
            result.text = ""
            currentNumber.text = ""
            performingMath = false
            numberOnscreen = 0
            preNumber = 0
            
        }
        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}

@IBDesignable class customButton: UIButton {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
}

